BATIER Léa

**TP SYSTEM**

Questions :
- Un flake nix est un répertoire avec un flake.nix et flake.lock à la racine qui génère des expressions Nix que d'autres peuvent utiliser pour faire des choses comme créer des packages, exécuter des programmes, utiliser des environnements de développement ou mettre en place des systèmes NixOS.

- Fichier flake.nix
```bash
#ce flake est une configuration pour créer un environnement de développement Nix avec des outils spécifiques. 
{
  description = "Open source system tp";
  #cette ligne définit une brève description du flake

  inputs = {
    #cette section spécifie les entrées nécessaires pour construire ce flake. Il dépend de deux autres flakes : nixpkgs et flake-utils

    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    #nixpkgs est un dépôt GitHub contenant la collection de paquets Nix de NixOS. Ce flake utilise la branche nixpkgs-unstable

    flake-utils.url = "github:numtide/flake-utils";
    #flake-utils est un autre flake qui est utilisé dans la configuration de Nix. Il est également obtenu à partir d'un dépôt GitHub (numtide/flake-utils).
  };

  outputs =
  #cette section spécifie les sorties générées par le flake. Les sorties sont généralement des attributs ou des éléments qui peuvent être utilisés dans d'autres configurations. Dans ce cas, les sorties sont définies comme une fonction qui prend plusieurs arguments (self, nixpkgs, flake-utils, etc.).

    { self 
    #self fait référence à ce flake lui-même.
    , nixpkgs
    #nixpkgs est une sortie qui correspond à l'importation du flake nixpkgs
    , flake-utils
    #flake-utils est une sortie qui correspond à l'importation du flake flake-utils
    , ... #"..."indique que d'autres sorties peuvent être spécifiées, mais elles ne sont pas explicitement définies dans le code
    }:

    flake-utils.lib.eachDefaultSystem (system:
    #eachDefaultSystem est une fonction de flake-utils qui itère sur les différents systèmes par défaut pris en charge par Nix 

    let
      pkgs = import nixpkgs {
        #"nixpkgs" importe, en utilisant le système spécifique actuellement en cours de traitement 
        inherit system;
      };
      tpPackages = [
        #création d'une liste de paquets appelée "tpPackages" qui contient plusieurs paquets Nix, tels que "go-task," "podman," "rustfmt," "trivy," et "hadolint."
        pkgs.go-task
        pkgs.podman
        pkgs.rustfmt
        pkgs.trivy
        pkgs.hadolint
      ];
    in
    {
      devShells.default =
      #sortie nommée devShells.default est définie. Cette sortie utilise la fonction mkShell pour créer un environnement de développement Nix contenant des nativeBuildInputs (dépendances de construction), y compris certains paquets de tpPackages et d'autres paquets tels que dapr-cli, stdenv, go, et glibc.static

        pkgs.mkShell
          {
            nativeBuildInputs = [
                
              pkgs.dapr-cli
              pkgs.stdenv
              pkgs.go
              pkgs.glibc.static
            ] ++ tpPackages;
          };
    });
}
```
- **Nixpkgs** est l’ensemble des milliers de packages pour le gestionnaire de packages Nix. Il implémente également NixOS.

- **Nixpkgs** est le référentiel central de NixOS contenant des milliers de définitions de paquets et de configurations pour les logiciels. C'est essentiel pour la création d'environnements logiciels reproductibles et personnalisables.

- La syntaxe est particulière car Nix utilise son propre langage.

- Mot clé Dockefile **CMD** : 
    CMD dans Dockerfile définit l'exécutable par défaut d'une image Docker. On peut exécuter cette image comme base d'un conteneur sans ajouter d'arguments de ligne de commande. Dans ce cas, le conteneur exécute le processus spécifié par la commande CMD.

- Mot clé Dockefile **ENTRYPOINT** :
    L'instruction ENTRYPOINT est utilisée pour configurer les exécutables qui s'exécuteront toujours après le lancement du conteneur. Par exemple, on peut mentionner un script à exécuter dès le démarrage du conteneur.

- Les **namespaces** sont une fonctionnalité du noyau Linux qui permet l'isolation et la séparation des ressources système entre les processus.

- Les **cgroups** sont une fonctionnalité du noyau Linux qui permet de gérer et de limiter les ressources système pour les processus et les groupes de processus. Ils ont été introduits dans le noyau Linux pour fournir un contrôle plus précis et une isolation des ressources, ce qui est essentiel pour la virtualisation, la conteneurisation et la gestion des ressources système.

- **Containerd** est un composant de l'écosystème de conteneurisation dans Linux, c'est un runtime de conteneurs open source qui offre une gestion de base des conteneurs, suit des normes ouvertes, est modulaire et extensible.

- Les **syscalls** qui sont utilisés pour faire tourner les containers : clone(), unshare(), setns(), chroot(), cgroup, prlimit(), mount() et umount(), sendfile(), socket(),bind(), setuid() et setgid().

- Un container alpine est intéressant par rapport debian ou ubuntu grâce à :
  - sa taille réduite, alpine est minimaliste, ce qui permet une rapidité de démarrage,
  - sa légerté permet de peu utiliser les ressources (RAM, CPU),
  - sa sécurité, étant donné qu'alpine est minismaliste il y a peu de logiciels installés par défaut,
  - sa flexibilité, on peut ajouter des paquets suplémentaires facilement pour adapter l'image à notre utilisation.

- Dans la distribution alpine **musl** est une bibliothèque standard C open source et légère utilisée comme alternative à la bibliothèque GNU C (glibc) dans de nombreuses distributions Linux, y compris Alpine Linux. 
