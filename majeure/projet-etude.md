# projet d'étude

## Présentation

L'open source propose une offre technologique trés vaste et diverse. Chacunes des solutions technique, chaque standard ou méthode issue de l'open source peu faire l'objet d'une étude d'approfondissement. cette étude apporte des compétences valorisable professionelement.

Chaque étudiant dans son projet de dévelopement de compétences s'intéressera à une technologie particulière et effectura une étude visant à developper ses compétences dans le domaine.

## Livrable

Cette étude aura pour résultat une présentation **par groupe** au reste de la classe.

Exemple de plan de présentation :

- Présentation de la solution
- Problematique soulevée par l'étude
- Cadre technique (ce qui é ata mis en oeuvre)
- Demonstration technique
- Conclusion

## Evaluation

Chaque groupe d'étudiant sera évaluer par chacuns des autres groupes d'étudiants et les professeurs selon 4 critères :

- Pertinence de l'étude, son l'intéret
- Challenge, la prise de risque, l'engagement
- Qualité technique des réalisations présenté (démo)
- Qualité de la présentation, la forme
